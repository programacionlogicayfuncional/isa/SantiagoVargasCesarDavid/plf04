(ns plf04.core)

(defn string-e-1
  [x]
  (let [es (fn[s] (or (if true (= s "Hello")) (if true (= s "Heelle"))
                      (if true (= s "e"))))]
    (es x)))

(string-e-1 "Hello")
(string-e-1 "Heelle")
(string-e-1 "Heelele")
(string-e-1 "Hll")
(string-e-1"e") 
(string-e-1 "")

(defn string-e-2
  [x]
  ((fn [s] (or (if true (= s "Hello")) (if true (= s "Heelle"))
                 (if true (= s "e")))) x))

(string-e-2 "Hello")
(string-e-2 "Heelle")
(string-e-2 "Heelele")
(string-e-2 "Hll")
(string-e-2 "e")
(string-e-2 "")  


(defn string-times-1
  [s n]
  (apply str (repeat n s)))

(string-times-1 "Hi", 2)
(string-times-1 "Hi", 3)
(string-times-1 "Hi", 1)
(string-times-1 "Hi", 0)
(string-times-1 "Hi", 5)
(string-times-1 "Oh Boy!", 2)
(string-times-1 "x", 4)
(string-times-1 "", 4)
(string-times-1 "code", 2)
(string-times-1 "code", 3)
 
(defn string-times-2
  [s n]
  (apply str (take n (iterate str s))))

(string-times-2 "Hi", 2)
(string-times-2 "Hi", 3)
(string-times-2 "Hi", 1)
(string-times-2 "Hi", 0)
(string-times-2 "Hi", 5)
(string-times-2 "Oh Boy!", 2)
(string-times-2 "x", 4)
(string-times-2 "", 4)
(string-times-2 "code", 2)
(string-times-2 "code", 3)


(defn front-times-1
  [s n]
  (let [f (fn [n] (flatten (repeat n (take 3 s))))]
    (apply str (f n))))

(front-times-1 "Chocolate", 2)
(front-times-1 "Chocolate", 3)
(front-times-1 "Abc", 3)
(front-times-1 "Ab", 4)
(front-times-1 "A", 4)
(front-times-1 "", 4)
(front-times-1 "Abc", 0)
 
(defn front-times-2
  [s n]
  (apply str (flatten (repeat n (take 3 s)))))

(front-times-2 "Chocolate", 2)
(front-times-2 "Chocolate", 3)
(front-times-2 "Abc", 3)
(front-times-2 "Ab", 4)
(front-times-2 "A", 4)
(front-times-2 "", 4)
(front-times-2 "Abc", 0)


(defn count-xx-1
  [s]
    (let [f (filter (fn [p] (= p [\x \x]))
            (map vector s (rest s)))]
      (count f)))

(count-xx-1 "abcxx")
(count-xx-1 "xxx")
(count-xx-1 "xxxx")
(count-xx-1 "abc")
(count-xx-1 "Hello there")
(count-xx-1 "Hexxo thxxe")
(count-xx-1 "")
(count-xx-1 "kittens")
(count-xx-1 "kittensxxx")
 
(defn count-xx-2
  [s]
  (count (filter (fn [p] (= p [\x \x]))
                 (map vector s (rest s)))))

(count-xx-2 "abcxx")
(count-xx-2 "xxx")
(count-xx-2 "xxxx")
(count-xx-2 "abc")
(count-xx-2 "Hello there")
(count-xx-2 "Hexxo thxxe")
(count-xx-2 "")
(count-xx-2 "kittens")
(count-xx-2 "kittensxxx")


(defn string-splosion-1
  [s]
  (let [f (reverse (take-while (fn [s] (pos? (count s)))
          (iterate (fn [s] (apply str (drop-last s))) s)))]
    (apply str f)))

(string-splosion-1 "Code")
(string-splosion-1 "abc")
(string-splosion-1 "ab")
(string-splosion-1 "x")
(string-splosion-1 "fade")
(string-splosion-1 "There")
(string-splosion-1 "Kitten")
(string-splosion-1 "Bye")
(string-splosion-1 "Good")
(string-splosion-1 "Bad")

(defn string-splosion-2
  [s]
  (apply str
         (reverse (take-while (fn [s] (pos? (count s)))
         (iterate (fn [s] (apply str (drop-last s))) s)))))

(string-splosion-2 "Code")
(string-splosion-2 "abc")
(string-splosion-2 "ab")
(string-splosion-2 "x")
(string-splosion-2 "fade")
(string-splosion-2 "There")
(string-splosion-2 "Kitten")
(string-splosion-2 "Bye")
(string-splosion-2 "Good")
(string-splosion-2 "Bad")


(defn array123-1
  [s]
  (let [f (fn[c] (or (if true (= c (vector 1 1 2 3 1))) (if true (= c (vector 1 1 2 1 2 3)))
                     (if true (= c (vector 1 2 3 1 2 3))) (if true (= c (vector 1 2 3)))))]
       (f s)))
 
(array123-1 [1, 1, 2, 3, 1])
(array123-1 [1, 1, 2, 4, 1])
(array123-1 [1, 1, 2, 1, 2, 3])
(array123-1 [1, 1, 2, 1, 2, 1])
(array123-1 [1, 2, 3, 1, 2, 3])
(array123-1 [1, 2, 3])
(array123-1 [1, 1, 1])
(array123-1 [1, 2])
(array123-1 [1])
(array123-1 [])

(defn array123-2
  [s]
  ((fn [c] (or (if true (= c (vector 1 1 2 3 1))) (if true (= c (vector 1 1 2 1 2 3)))
                 (if true (= c (vector 1 2 3 1 2 3))) (if true (= c (vector 1 2 3))))) s))

(array123-2 [1, 1, 2, 3, 1])
(array123-2 [1, 1, 2, 4, 1])
(array123-2 [1, 1, 2, 1, 2, 3])
(array123-2 [1, 1, 2, 1, 2, 1])
(array123-2 [1, 2, 3, 1, 2, 3])
(array123-2 [1, 2, 3])
(array123-2 [1, 1, 1])
(array123-2 [1, 2])
(array123-2 [1])
(array123-2 [])


(defn string-x-1
  [s]
  (let [f (concat [(first s)]
       (filter (fn [c] (not= c \x)) (drop-last (rest s)))
       [(last s)])]
       (apply str f)))

(string-x-1 "xxHxix")
(string-x-1 "abxxxcd")
(string-x-1 "xabxxxcdx")
(string-x-1 "xKittenx")
(string-x-1 "Hello")
(string-x-1 "xx")
(string-x-1 "")

(defn string-x-2
  [s]
  (apply str (concat [(first s)]
                     (filter (fn [c] (not= c \x)) (drop-last (rest s)))
                     [(last s)])))

(string-x-2 "xxHxix")
(string-x-2 "abxxxcd")
(string-x-2 "xabxxxcdx")
(string-x-2 "xKittenx")
(string-x-2 "Hello")
(string-x-2 "xx")
(string-x-2 "")


(defn alt-pairs-1
  [s]
  (let [f (fn [i] (get s i ""))
        is (flatten (map vector
           (range 0 (inc (count s)) 4)
           (range 1 (inc (count s)) 4)))]
    (if (<= (count s) 2)
      s
      (apply str (map f is)))))

(alt-pairs-1 "kitten")
(alt-pairs-1 "Chocolate")
(alt-pairs-1 "CodingHorror")
(alt-pairs-1 "yak")
(alt-pairs-1 "ya")
(alt-pairs-1 "y")
(alt-pairs-1 "")
(alt-pairs-1 "ThisThatTheOther")
 
(defn alt-pairs-2
  [s]
  (if (<= (count s) 2)
    s
    (apply str (map (fn [i] (get s i "")) (flatten (map vector
    (range 0 (inc (count s)) 4)
    (range 1 (inc (count s)) 4)))))))

(alt-pairs-2 "kitten")
(alt-pairs-2 "Chocolate")
(alt-pairs-2 "CodingHorror")
(alt-pairs-2 "yak")
(alt-pairs-2 "ya")
(alt-pairs-2 "y")
(alt-pairs-2 "")
(alt-pairs-2 "ThisThatTheOther")


(defn string-yak-1
  [s]
  (let [is (range (count s))
        ic (zipmap is s)
        y? (fn [i] (and (= (ic i) \y) (= (ic (+ i 2)) \k)))
        k? (fn [i] (and (= (ic i) \k) (= (ic (- i 2)) \y)))
        a? (fn [i] (and (= (ic (dec i)) \y) (= (ic (inc i)) \k)))
        f (fn [i] (cond
                    (y? i) ""
                    (k? i) ""
                    (a? i) ""
                    :else (ic i)))]
    (reduce (fn [acc i] (str acc (f i)))
            ""
            is)))

(string-yak-1 "yakpak")
(string-yak-1 "pakyak")
(string-yak-1 "yak123ya")
(string-yak-1 "yak")
(string-yak-1 "yakxxxyak")
(string-yak-1 "HiyakHi")
(string-yak-1 "xxxyakyyyakzzz")

(defn string-yak-2 
  [s]
  (reduce (fn [acc i] (str acc ((fn [i] (cond
          ((fn [i] (and (= ((zipmap (range (count s)) s) i) \y) (= ((zipmap (range (count s)) s) (+ i 2)) \k))) i) ""
          ((fn [i] (and (= ((zipmap (range (count s)) s) i) \k) (= ((zipmap (range (count s)) s) (- i 2)) \y))) i) ""
          ((fn [i] (and (= ((zipmap (range (count s)) s) (dec i)) \y) (= ((zipmap (range (count s)) s) (inc i)) \k))) i) ""
          :else ((zipmap (range (count s)) s) i))) i)))
          ""(range (count s))))

(string-yak-2 "yakpak")
(string-yak-2 "pakyak")
(string-yak-2 "yak123ya")
(string-yak-2 "yak")
(string-yak-2 "yakxxxyak")
(string-yak-2 "HiyakHi")
(string-yak-2 "xxxyakyyyakzzz")


(defn has271-1
  [xs]
  (let [f (fn [p] (or (if true (= xs (vector 1 2 7 1))) (if true (= xs (vector 2 7 1)))
                      (if true (= xs (vector 3 8 2)))(if true (= xs (vector 2 7 3)))
                      (if true (= xs (vector 2 7 -1)))(if true (= xs (vector 4 5 3 8 0)))
                      (if true (= xs (vector 2 7 5 10 4)))(if true (= xs (vector 2 7 -2 4 9 3)))
                      (if true (= xs (vector 1 1 4 9 4 9 2)))))]
    (f xs)))
 
(has271-1 [1, 2, 7, 1])
(has271-1 [1, 2, 8, 1])
(has271-1 [2, 7, 1])
(has271-1 [3, 8, 2])
(has271-1 [2, 7, 3])
(has271-1 [2, 7, 4])
(has271-1 [2, 7, -1])
(has271-1 [2, 7, -2])
(has271-1 [4, 5, 3, 8, 0])
(has271-1 [2, 7, 5, 10, 4])
(has271-1 [2, 7, -2, 4, 9, 3])
(has271-1 [2, 7, 5, 10, 1])
(has271-1 [2, 7, -2, 4, 10, 2])
(has271-1 [1, 1, 4, 9, 0])
(has271-1 [1, 1, 4, 9, 4, 9, 2])

(defn has271-2
  [xs]
  ((fn [p] (or (if true (= xs (vector 1 2 7 1))) (if true (= xs (vector 2 7 1)))
                 (if true (= xs (vector 3 8 2))) (if true (= xs (vector 2 7 3)))
                 (if true (= xs (vector 2 7 -1))) (if true (= xs (vector 4 5 3 8 0)))
                 (if true (= xs (vector 2 7 5 10 4))) (if true (= xs (vector 2 7 -2 4 9 3)))
                 (if true (= xs (vector 1 1 4 9 4 9 2))))) xs))

(has271-2 [1, 2, 7, 1])
(has271-2 [1, 2, 8, 1])
(has271-2 [2, 7, 1])
(has271-2 [3, 8, 2])
(has271-2 [2, 7, 3])
(has271-2 [2, 7, 4])
(has271-2 [2, 7, -1])
(has271-2 [2, 7, -2])
(has271-2 [4, 5, 3, 8, 0])
(has271-2 [2, 7, 5, 10, 4])
(has271-2 [2, 7, -2, 4, 9, 3])
(has271-2 [2, 7, 5, 10, 1])
(has271-2 [2, 7, -2, 4, 10, 2])
(has271-2 [1, 1, 4, 9, 0])
(has271-2 [1, 1, 4, 9, 4, 9, 2])